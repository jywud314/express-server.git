const mongoose = require('mongoose');
const { host, database } = require('./config');

mongoose.connect('mongodb://' + host + '/' + database, function (err) {
  if (err) console.log('mongodb 连接数据库失败');
  else console.log('mongodb 连接数据库成功');
});

module.exports = mongoose;
