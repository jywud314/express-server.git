const $mysql = require('mysql');
const config = require('./config.js');
const connection = $mysql.createConnection(config);
connection.connect(err => {
  // err代表失败
  if (err) {
    console.log('mysql 连接失败');
  } else {
    console.log('mysql 连接成功');
  }
});

//关闭连接
// connection.end();

module.exports = connection;
