const connection = {
  host: 'localhost', //mysql的安装主机地址
  user: 'root', //访问mysql的用户名
  password: '123456', // 访问mysql的密码
  database: 'test_db' //访问mysql的数据库名
};

module.exports = connection;
