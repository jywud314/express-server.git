const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const partials = require('express-partials');

const config = require('./config');
const routes = require('./routes/index');

const app = express();

const port = process.env.PORT || config.port;
app.set('port', port);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// 静态文件托管
app.use(express.static(path.join(__dirname, 'public')));

app.use(logger('dev'));

app.use(bodyParser.json());
//extended:false 方法内部使用querystring模块处理请求参数的格式
//extended:true 方法内部使用第三方模块qs处理请求参数的格式
app.use(bodyParser.urlencoded({ extended: false }));
app.use(partials());
app.use(cookieParser());

//配置diskStorage来控制文件存储的位置以及文件名字等

app.use('/', routes);

app.listen(app.get('port'), () => {
  console.log('Program starts running ...');
  console.log('server run success on port ' + app.get('port'));
});

module.exports = app;
